package com.test.javers.controllers;

import static java.util.Collections.singletonMap;
import static org.javers.repository.jql.QueryBuilder.byInstanceId;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import static org.springframework.http.ResponseEntity.notFound;

import java.util.stream.Stream;

import org.javers.core.Changes;
import org.javers.core.Javers;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.javers.services.Entity;
import com.test.javers.services.EntityService;

@RestController
@RequestMapping(path = "/entity", produces = APPLICATION_JSON_VALUE)
public class EntityController {
	private final Javers javers;
	private final EntityService service;

	public EntityController(final Javers javers, final EntityService service) {
		this.javers = javers;
		this.service = service;
	}

	@PostMapping
	public Object create(@RequestBody final Entity entity) {
		return singletonMap("created", this.service.create(entity));
	}

	@DeleteMapping("{id}")
	public Object delete(@PathVariable final String id) {
		return singletonMap("deleted", this.service.delete(id));
	}

	@GetMapping
	public Stream<EntityModel<Entity>> read() {
		return this.service.read()
				.map(id -> EntityModel.of(new Entity(id))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).read(id))
								.withRel("read")
								.withType("GET"))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).readChanges(id))
								.withRel("read")
								.withType("GET"))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).readShadows(id))
								.withRel("read")
								.withType("GET"))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).readSnapshots(id))
								.withRel("read")
								.withType("GET")));
	}

	@GetMapping("{id}")
	public ResponseEntity<EntityModel<Entity>> read(@PathVariable final String id) {
		return this.service.read(id)
				.map(entity -> EntityModel.of(entity)
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).update(entity))
								.withRel("update")
								.withType("PUT"))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).delete(id))
								.withRel("delete")
								.withType("DELETE")))
				.map(ResponseEntity::ok)
				.orElse(notFound().build());
	}

	@GetMapping("{id}/changes")
	public Changes readChanges(@PathVariable final String id) {
		return this.javers.findChanges(byInstanceId(id, Entity.class).build());
	}

	@GetMapping(path = "{id}/changes", consumes = TEXT_PLAIN_VALUE, produces = TEXT_PLAIN_VALUE)
	public String readChangesText(@PathVariable final String id) {
		return this.readChanges(id)
				.prettyPrint();
	}

	@GetMapping("{id}/shadows")
	public Object readShadows(@PathVariable final String id) {
		return this.javers.findShadows(byInstanceId(id, Entity.class).build());
	}

	@GetMapping("{id}/snapshots")
	public Object readSnapshots(@PathVariable final String id) {
		return this.javers.findSnapshots(byInstanceId(id, Entity.class).build());
	}

	@PutMapping
	public Object update(@RequestBody final Entity entity) {
		return singletonMap("updated", this.service.update(entity));
	}
}