package com.test.javers.controllers;

import static org.javers.repository.jql.QueryBuilder.byClass;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.stream.Stream;

import org.javers.core.Javers;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.javers.services.Entity;

@RestController
@RequestMapping(path = "/javers", produces = APPLICATION_JSON_VALUE)
public class JaversController {
	private final Javers javers;

	public JaversController(final Javers javers) {
		this.javers = javers;
	}

	@GetMapping
	public Stream<EntityModel<Entity>> read() {
		return this.javers.findChanges(byClass(Entity.class).build())
				.stream()
				.map(change -> (String) change.getAffectedLocalId())
				.distinct()
				.map(id -> EntityModel.of(new Entity(id))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).readChanges(id))
								.withRel("read")
								.withType("GET"))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).readShadows(id))
								.withRel("read")
								.withType("GET"))
						.add(WebMvcLinkBuilder.linkTo(methodOn(EntityController.class).readSnapshots(id))
								.withRel("read")
								.withType("GET")));
	}
}