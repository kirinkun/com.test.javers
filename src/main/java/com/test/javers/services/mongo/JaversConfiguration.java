package com.test.javers.services.mongo;

import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.metamodel.clazz.EntityDefinitionBuilder;
import org.javers.repository.mongo.MongoRepository;
import org.javers.spring.auditable.MockAuthorProvider;
import org.javers.spring.auditable.aspect.JaversAuditableAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.client.MongoDatabase;
import com.test.javers.services.Entity;

@Configuration
public class JaversConfiguration {
	@Bean
	public Javers javers(final MongoDatabase database) {
		return JaversBuilder.javers()
				.registerEntity(EntityDefinitionBuilder.entityDefinition(Entity.class)
						.withIdPropertyName("id")
						.build())
				.registerJaversRepository(new MongoRepository(database))
				.build();
	}

	@Bean
	public JaversAuditableAspect javersAuditableAspect(final Javers javers) {
		return new JaversAuditableAspect(javers, new MockAuthorProvider());
	}
}
