package com.test.javers.services.mongo;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import org.bson.Document;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.test.javers.services.Entity;

@Configuration
public class MongoConfiguration {
	@Bean
	public MongoCollection<Entity> collection(final MongoDatabase database) {
		return database.getCollection("entity", Entity.class)
				.withCodecRegistry(fromRegistries(getDefaultCodecRegistry(), fromProviders(PojoCodecProvider.builder()
						.register(Entity.class)
						.build())));
	}

	@Bean
	public MongoDatabase database(@Value("${mongo.uri}") final String uri) {
		return MongoClients.create(uri)
				.getDatabase("main");
	}

	@Bean
	public MongoCollection<Document> document(final MongoDatabase database) {
		return database.getCollection("entity");
	}
}
