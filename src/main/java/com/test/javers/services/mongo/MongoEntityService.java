package com.test.javers.services.mongo;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Updates.unset;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;
import static org.bson.types.ObjectId.isValid;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.javers.spring.annotation.JaversAuditable;
import org.javers.spring.annotation.JaversAuditableDelete;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import com.test.javers.services.Entity;
import com.test.javers.services.EntityService;

@Service
public class MongoEntityService implements EntityService {
	private final MongoCollection<Document> collection;

	public MongoEntityService(final MongoCollection<Document> collection) {
		this.collection = collection;
	}

	@Override
	@JaversAuditable
	public String create(final Entity entity) {
		final String id = this.collection.insertOne(asDocument(entity))
				.getInsertedId()
				.asObjectId()
				.getValue()
				.toHexString();
		entity.setId(id);
		return id;
	}

	@Override
	@JaversAuditableDelete(entity = Entity.class)
	public long delete(final String id) {
		return this.collection.deleteOne(filterBy(id))
				.getDeletedCount();
	}

	@Override
	public Stream<String> read() {
		return stream(this.collection.find()
				.projection(include("_id"))
				.spliterator(), false)
						.map(document -> document.getObjectId("_id")
								.toHexString());
	}

	@Override
	public Optional<Entity> read(final String id) {
		return stream(this.collection.find(filterBy(id))
				.spliterator(), false).map(MongoEntityService::asEntity)
						.findFirst();
	}

	@Override
	@JaversAuditable
	public long update(final Entity entity) {
		return this.collection.updateOne(filterBy(entity), combine(updatesFrom(entity)), new UpdateOptions().upsert(false))
				.getModifiedCount();
	}

	private static Document asDocument(final Entity entity) {
		final var document = new Document(entity.getProperties());
		if (entity.getId() != null && isValid(entity.getId())) {
			document.put("_id", new ObjectId(entity.getId()));
		}
		document.put("name", entity.getName());
		return document;
	}

	private static Entity asEntity(final Document document) {
		final var entity = new Entity();
		document.forEach(entity::setProperty);
		final var properties = entity.getProperties();
		entity.setId(((ObjectId) properties.remove("_id")).toHexString());
		entity.setName((String) properties.remove("name"));
		return entity;
	}

	private static Bson filterBy(final Entity entity) {
		return filterBy(entity.getId());
	}

	private static Bson filterBy(final String id) {
		if (id != null && isValid(id)) {
			return eq(new ObjectId(id));
		} else {
			return eq(id);
		}
	}

	private static List<Bson> updatesFrom(final Entity entity) {
		final var updates = entity.getProperties()
				.entrySet()
				.stream()
				.map(entry -> {
					final String key = entry.getKey();
					if (key.startsWith("-")) {
						return unset(key.substring(1));
					} else {
						return set(key, entry.getValue());
					}
				})
				.collect(toList());
		if (entity.getName() != null && !entity.getName()
				.isBlank()) {
			updates.add(set("name", entity.getName()));
		}
		return updates;
	}
}
