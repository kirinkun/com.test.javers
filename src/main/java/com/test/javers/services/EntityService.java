package com.test.javers.services;

import java.util.Optional;
import java.util.stream.Stream;

public interface EntityService {
	String create(Entity entity);

	long delete(String id);

	Stream<String> read();

	Optional<Entity> read(String id);

	long update(Entity entity);
}
