package com.test.javers.services;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonInclude(NON_EMPTY)
@JsonNaming(SnakeCaseStrategy.class)
public class Entity {
	private String id;
	private String name;
	private final Map<String, Object> properties = new HashMap<>();

	public Entity() {
	}

	public Entity(final String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	@JsonAnyGetter
	public Map<String, Object> getProperties() {
		return this.properties;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@JsonAnySetter
	public void setProperty(final String key, final Object value) {
		this.properties.put(key, value);
	}
}
